
import sys
from PyQt4.QtCore import QUrl
from PyQt4.QtGui import QApplication
from PyQt4.QtWebKit import QWebPage

# note: due to problems installing/importing PyQt4 and PyQt5 using a
# virtual environment, the standard interpreter is used


class DynamicClient(QWebPage):
    """Web client (to act as a web browser) that will load dynamic
    Javascript in a web page. Code taken from YouTube video 'Dynamic
    Javascript Scraping - Web scraping with Beautiful Soup 4 p.4',

    https://www.youtube.com/watch?v=FSH77vnOGqU .
    """
    def __init__(self, url):
        self.app = QApplication(sys.argv)
        QWebPage.__init__(self)
        self.loadFinished.connect(self.on_page_load)
        self.mainFrame().load(QUrl(url))
        self.app.exec_()

    def on_page_load(self):
        self.app.quit()
