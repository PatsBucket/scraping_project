#!/usr/bin/env python

from datetime import datetime
import pytz
import requests
from bs4 import BeautifulSoup
from pymongo import MongoClient


def get_rankings(wanted_sites, search_term, search_engine='.co.uk',
                 n_results=100, write_to_db=False):
    """Get rankings from a search using Google.

    :param wanted_sites: (list of str) websites of interest
    :param search_term: (str)
    :param search_engine: (str) e.g. '.co.uk' or '.com'
    :param n_results: (int) number of search results to consider
    :param write_to_db: (bool) whether to write to data base

    :return rankings: (dict) e.g. rank 1 would be first, etc
    """
    if search_engine == '.co.uk':
        gl = 'uk'
        lr = 'lang_en'
    elif search_engine == '.com':
        gl = 'us'
        lr = 'lang_en'
    else:
        raise NotImplementedError('unsupported search_engine {}'
                                  .format(search_engine))

    if not isinstance(wanted_sites, list):
        wanted_sites = [wanted_sites]

    url = ('https://www.google{}/search?pws=0&gl={}&lr={}&num={}&q='
           .format(search_engine, gl, lr, n_results + 25))
    url += search_term.replace(' ', '+')
    # note: pws=0 helps de-personalise the search

    response = requests.get(url=url, timeout=10)
    # TODO: implement re-tries? (see Scraping for Fun and Profit blog)
    now = datetime.now(tz=pytz.utc)
    soup = BeautifulSoup(response.content, 'lxml')

    search_results = soup.find_all('h3', class_='r')

    search_results = _parse_search_results(search_results=search_results)
    search_results = search_results[:n_results]

    n_search_results = len(search_results)
    print ('number of search results found: {}'.format(n_search_results))

    if n_search_results == 0:
        print 'no results returned'
        return {}

    # get ranking of each wanted site
    print ('search results: {}'.format(search_results))
    rankings = _extract_rankings(wanted_sites=wanted_sites,
                                 search_results=search_results)

    if write_to_db:
        _write_to_db(rankings=rankings, search_term=search_term,
                     search_engine=search_engine, n_results=n_results,
                     now=now, wanted_sites_only=True)

    return rankings


def _parse_search_results(search_results):
    # try:
    #     # parse search_results to extract wanted urls
    #     search_results = [
    #         r.find('a', href=True)['href'].split('=')[1].split('/')[2]
    #         for r in search_results]
    # except:
    #     return None  # indicate problem with search
    #     # TODO: consider sending an email to report problem

    # parse search_results to extract wanted urls
    search_results_ = []  # initialise
    for sr in search_results:
        try:
            r = sr.find('a', href=True)['href']
            if r.startswith('/url?q'):
                r = r.split('=')
                r = r[1]
                r = r.split('/')
                r = r[2]
                search_results_ += [r]
            elif r.startswith('/search?q'):
                pass  # search info: ignore
            elif r.startswith('https://books.google.'):
                pass  # book (not website) result: ignore
            else:
                raise Exception('unrecognised search result')
        except:
            print ('problem parsing (ignoring) search result: {} {}'
                   .format(r, sr))
            pass  # do nothing by assuming that search result was not a site

    return search_results_


def _extract_rankings(wanted_sites, search_results):
    split_search_results = [r.split('.') for r in search_results]
    rankings = {}
    for wanted_site in wanted_sites:
        split_wanted_site = wanted_site.split('.')
        ranking_ = [
            ssr_i + 1  # e.g. rank 1 is first, etc
            for ssr_i, ssr in enumerate(split_search_results)
            if all([ws in ssr for ws in split_wanted_site])]

        # record ranking
        n_matches_ = len(ranking_)
        if n_matches_ > 0:
            rankings[wanted_site] = ranking_[0]
        else:
            # no matches in search results
            rankings[wanted_site] = -99

    return rankings


def _write_to_db(rankings, search_term, search_engine, n_results, now,
                 wanted_sites_only=True):
    db_client = MongoClient()

    for k, v in rankings.iteritems():
        r = {
            'search_term': search_term,
            'search_engine': search_engine,
            'datetime': now,  # record datetime
            'n_search_results': n_results,
            'site_ranked': k,
            'site_rank': v,
        }
        db_client.google_search_rankings.rankings.update(r, r, upsert=True)

    # db = MySQLdb.connect(host="77.72.4.82",
    #                      user="scriptswebwiseme_grankers",
    #                      passwd="s%h@r,54l}eH",
    #                      db='scriptswebwiseme_sem')

    if not wanted_sites_only:
        raise Exception('unsupported code')

        results = {str(i + 1): r for i, r in enumerate(search_results)}
        # note: e.g. {'1': ...} is the first result, etc
        db_client.google_search_rankings.rankings.update(results, results,
                                                         upsert=True)
        # TODO: structure data base properly

        reference_info = {
            'search_term': search_term,
            'search_engine': search_engine,
            'datetime': now,  # record datetime
            'n_results': n_results,
        }
        db_client.google_search_rankings.rankings.update(
            reference_info, reference_info, upsert=True)


if __name__ == '__main__':
    # rankings = get_rankings(
    #     'webwisemedia.co.uk',
    #     'web developers exeter', write_to_db=False)
    rankings = get_rankings(
        ['webwisemedia.co.uk', 'optixsolutions.co.uk', 'illicitwebdesign.co.uk', 'daneswood.co.uk'],
        'website design Exeter', write_to_db=False)
    print rankings
