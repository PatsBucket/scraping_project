#!/usr/bin/env python

import argparse
# from google_rankings.get_rankings import get_rankings
from get_rankings import get_rankings


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument(
        '-wanted_sites', nargs='+', required=True,
        help='website(s) of interest, e.g. webwisemedia.co.uk')

    arg_parser.add_argument(
        '-search_term', required=True, help='e.g. web developers exeter')

    arg_parser.add_argument(
        '-search_engine', required=True, help='e.g. .co.uk')

    arg_parser.add_argument(
        '-n_results', default=100, help='number of search results to consider')

    arg_parser.add_argument('-write_to_db', default=False)
    args = arg_parser.parse_args()

    rankings = get_rankings(
        wanted_sites=args.wanted_sites,
        search_term=args.search_term,
        search_engine=args.search_engine,
        n_results=args.n_results,
        write_to_db=args.write_to_db)
    print rankings
