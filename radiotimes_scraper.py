
import logging
from datetime import datetime
import time
from pymongo import MongoClient
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from bs4 import BeautifulSoup


FILM_KEY = {
    'three billboards outside ebbing missouri': 'fpqhfg',
    'watership down': 'fndjtf',
}
# TODO: automatically get film id from somewhere on Radiotimes website


class RadioTimesFilmScraper(object):
    def __init__(self):
        logging.basicConfig(level=logging.INFO)
        self.logger = logging.getLogger(__name__)
        self.logger.info('initialising a {} instance'
                         .format(self.__class__.__name__))

        # helpers for properties
        self._db_client = None
        self._web_client = None

    @property
    def db_client(self):
        if self._db_client is None:
            self._db_client = MongoClient()
            self.logger.info('opened data base client')

        return self._db_client

    def close_db_client(self):
        if self._db_client is not None:
            self.db_client.close()
            self.logger.info('closed data base client')

    @property
    def web_client(self):
        if self._web_client is None:
            chrome_options = Options()
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--window-size=1920x1080')
            executable_path = ChromeDriverManager().install()
            self._web_client = webdriver.Chrome(
                options=chrome_options, executable_path=executable_path)
            self.logger.info('opened web client')

        return self._web_client

    def close_web_client(self):
        if self._web_client is not None:
            self._web_client.close()
            self.logger.info('closed web client')

    def scrape(self, films, use_film_ids=False, write_to_db=False,
               close_clients=True):
        """Run scraper.

        :param films: (list of str) film names (or ids if use_film_ids==True)
        :param use_film_ids: (bool) interpret 'films' param as RadioTimes ids
        :param write_to_db: (bool) whether to write to data base
        :param close_clients: (bool) whether to close open client properties
        :return: (dict) film star ratings
        """
        if not isinstance(films, list):
            films = [films]  # handle single film

        star_dict = {}  # initialise
        for film in films:
            stars = self._scrape_single_film(
                film=film, use_film_id=use_film_ids, write_to_db=write_to_db)

            if stars is not None:
                star_dict[film] = stars

        if close_clients:
            self.close_db_client()
            self._db_client = None  # reset in case instance is used later

            self.close_web_client()
            self._web_client = None

        self.logger.info('results: {}'.format(star_dict))
        return star_dict

    def _scrape_single_film(self, film, use_film_id, write_to_db):
        self.logger.info('starting scraper for film {}...'.format(film))
        film_id = self._get_film_id(film, use_film_id)

        if write_to_db:
            self.logger.info('checking whether film is in data base')
            db_response = self.db_client.srcaping.radiotimes
            film_from_db = db_response.find_one({'film_id': film_id})
            if film_from_db:
                self.logger.info('film found in data base; exiting scraper')
                return None

        url = ("https://www.radiotimes.com/film/{}/".format(film_id))
        self.web_client.get(url=url)
        now = datetime.now()  # record time of scraping
        time.sleep(2)  # guard against bombarding website

        soup = BeautifulSoup(self._web_client.page_source, "lxml")

        # find film name on webpage
        film_name = self._get_film_name_from_soup(soup=soup)

        # check that film is found on website
        titles_ = soup.find_all('title')
        if any(['page seems to be missing' in t.contents[0] for t in titles_]):
            self.logger.info('no web page found; exiting scraper')
            return None

        stars = self._get_stars_from_soup(soup=soup)
        self.logger.info('stars: {}'.format(stars))

        if write_to_db:
            self.logger.info('writing to data base')
            db_response.insert_one({
                'film_name': film_name,
                'stars': stars,
                'film_id': film_id,
                'time_collected': now,
            })
            self.logger.info('written to data base')

        self.logger.info('...finished scraping film {}'.format(film))
        return stars

    @staticmethod
    def _get_film_id(film, use_film_id):
        if use_film_id:
            film_id = film
        else:
            if film not in FILM_KEY:
                raise ValueError('unrecognised film {}'.format(film))

            film_id = FILM_KEY[film.lower()]

        return film_id

    def _get_film_name_from_soup(self, soup):
        film_name = soup.find_all('h1')
        assert (len(film_name) == 1)
        film_name = film_name[0]
        film_name = film_name.contents
        assert (len(film_name) == 1)
        film_name = film_name[0]
        film_name = film_name.strip()
        self.logger.info('film name found on web page is {}'.format(film_name))
        return film_name

    @staticmethod
    def _get_stars_from_soup(soup):
        stars_list = soup.find_all("span", class_="isvg-star")
        assert (len(stars_list) == 5)

        stars = 0  # initialise
        for star in stars_list:
            star = star.get("class")
            assert (len(star) == 2)
            star = star[1]
            star = star.split("--")
            assert (len(star) == 2)
            star = star[1]
            assert (star in ["filled", "empty"])
            if star == "filled":
                stars += 1

        return stars


if __name__ == '__main__':
    films = list(FILM_KEY.keys())

    scraper = RadioTimesFilmScraper()
    stars_dict = scraper.scrape(films=films, write_to_db=False)
