#!/usr/bin/env python

import numpy as np
import pandas as pd
from datetime import datetime
import pytz
from bs4 import BeautifulSoup


class PinnacleOddsBase(object):
    def __init__(self):
        self._comp = None
        self._url = None
        self._scrape_tuples = None
        self._req_fixture_info_len = None
        self._supremacy_mkt_cols = None
        self._cols_to_numeric = None
        # TODO: make these hidden attributes properties?

    @property
    def sport(self):
        raise NotImplementedError('implement in child class')

    def scrape(self, method='selenium', write_to_db=False, csv_file=None):
        """Scrape odds.

        :param write_to_db: (bool)
            whether to write to data base
        :param csv_file: (None or str)
            if not None, file to write data out as a .csv

        :return fixture_info_df: (pd.DataFrame)
        """
        assert (self._url is not None)

        source_html, now = self._get_source_html(method=method)
        soup = BeautifulSoup(source_html, 'lxml')

        fixture_info_dict = self._get_fixture_info_dict(soup)
        fixture_info_df = self._parse_fixture_info_dict(fixture_info_dict)

        fixture_info_df['datetime'] = now

        if write_to_db:
            self._write_to_db(fixture_info_df=fixture_info_df)

        if csv_file is not None:
            fixture_info_df.to_csv(csv_file)

        return fixture_info_df

    def _get_source_html(self, method):
        if method == 'dynamic_client':
            from dynamic_client import DynamicClient

            client_response = DynamicClient(url=self._url)
            now = datetime.now(tz=pytz.utc)

            source_html = client_response.mainFrame().toHtml()
            source_html = unicode(source_html)

        elif method == 'selenium':
            from selenium import webdriver
            from selenium.webdriver.chrome.options import Options
            from webdriver_manager.chrome import ChromeDriverManager

            chrome_options = Options()
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--window-size=1920x1080')
            executable_path = ChromeDriverManager().install()
            client_response = webdriver.Chrome(
                chrome_options=chrome_options, executable_path=executable_path)
            client_response.get(url=self._url)
            now = datetime.now(tz=pytz.utc)

            source_html = client_response.page_source
            client_response.close()

        elif method == 'selenium_firefox':
            from pyvirtualdisplay import Display
            from selenium import webdriver

            display = Display(visible=0, size=(800, 600))
            display.start()

            try:
                client_response = webdriver.Firefox()
                client_response.get(url=self._url)
                now = datetime.now(tz=pytz.utc)
                source_html = client_response.page_source

            finally:
                # tidy-up
                client_response.quit()
                display.stop()  # note: ignore any output from this

        else:
            raise ValueError('unrecognised method {}'.format(method))

        return source_html, now

    def _get_fixture_info_dict(self, soup):
        assert (self._scrape_tuples is not None)
        assert (self._req_fixture_info_len is not None)

        info_on_dates = soup.find_all(
            'div', class_='ng-scope',
            attrs={'ng-repeat': 'date in currentPeriod.dates'})

        fixture_info_dict = {}  # initialise
        for info_on_date in info_on_dates:
            date_info = self._extract_date_info(info_on_date=info_on_date)
            fixture_infos = info_on_date.find_all('tbody', class_='ng-scope')

            # loop through fixtures
            for fixture_info in fixture_infos:
                fixture_info_ = self._extract_fixture_info(
                    fixture_info=fixture_info, date_info=date_info)

                if fixture_info_['side1'] or fixture_info_['side2']:
                    # add fixture information to fixture_info_dict
                    fixture_info_dict[len(fixture_info_dict)] = fixture_info_

        return fixture_info_dict

    def _extract_date_info(self, info_on_date):
        date_info = info_on_date.find_all('span', class_='date-highlight')
        assert (len(date_info) == 1)
        date_info = date_info[0].text.strip()
        return date_info

    def _extract_fixture_info(self, fixture_info, date_info):
        fixture_info_ = {'fixture_date': date_info,
                         'side1': {}, 'side2': {}}  # initialise

        for label, class_ in self._scrape_tuples:
            fi = fixture_info.find_all('td', class_=class_)
            if len(fi) not in self._req_fixture_info_len:
                break

            # loop through teams
            assert (len(fi) in self._req_fixture_info_len)  # ensure 'break' above is present
            for side_label, side_i in [('side1', 0), ('side2', 1)]:
                if label in ['team_name', 'moneyline']:
                    side_fi = fi[side_i].find_all(
                        'span', class_='ng-binding ng-scope')
                    if len(side_fi) != 1:
                        break

                    assert (len(side_fi) == 1)  # ensure 'break' above is present
                    side_fi = side_fi[0]
                    side_fi = side_fi.text.strip()

                    fixture_info_[side_label][label] = side_fi

                elif label in ['handicap']:
                    for info_label in ['spread', 'price']:
                        side_fi = fi[side_i].find_all(
                            'span', class_='{} ng-binding'.format(info_label))
                        if len(side_fi) != 1:
                            break

                        assert (len(side_fi) == 1)  # ensure 'break' above is present
                        side_fi = side_fi[0]
                        side_fi = side_fi.text.strip()

                        fixture_info_[side_label][
                            '_'.join([label, info_label])] = side_fi

                elif label in ['totals']:
                    side_fi = fi[side_i].find_all('span', class_='ng-binding')
                    if len(side_fi) != 2:
                        break

                    assert (len(side_fi) == 2)  # ensure 'break' above is present
                    for info_label, info_i in [('spread', 0), ('price', 1)]:
                        fixture_info_[side_label][
                            '_'.join([label, info_label])] = side_fi[
                            info_i].text.strip()

                else:
                    raise ValueError('unrecognised label {}'.format(label))

        return fixture_info_

    def _parse_fixture_info_dict(self, fixture_info_dict):
        """Parse dict of fixture information into a pd.DataFrame.

        :param fixture_info_dict: (dict) fixture information
        :return: (pd.DataFrame) fixture_info_df
        """
        assert (self._comp is not None)
        assert (self._supremacy_mkt_cols is not None)
        assert (self._cols_to_numeric is not None)

        fixture_keys, fixture_values = self._get_lists_from_dict(
            fixture_info_dict=fixture_info_dict)

        fixture_info_df = self._convert_dict_to_df(
            fixture_keys=fixture_keys, fixture_values=fixture_values)

        return fixture_info_df

    @staticmethod
    def _get_lists_from_dict(fixture_info_dict):
        fixture_keys = []
        fixture_values = []
        for k, v in fixture_info_dict.iteritems():
            fixture_keys += [k]
            fixture_values += [v]

        return fixture_keys, fixture_values

    def _convert_dict_to_df(self, fixture_keys, fixture_values):
        # initialise data frame
        fixture_info_df = pd.DataFrame({
            'fixture_date': [v.get('fixture_date', np.nan)
                             for v in fixture_values],
            'comp': self._comp,
            'team1_name': [v.get('side1', {}).get('team_name', np.nan)
                           for v in fixture_values],
            'team2_name': [v.get('side2', {}).get('team_name', np.nan)
                           for v in fixture_values],
        }, index=fixture_keys)

        # ensure order of columns
        fixture_info_df = fixture_info_df[
            ['fixture_date', 'comp', 'team1_name', 'team2_name']]

        # fill out data frame
        for info in self._supremacy_mkt_cols:
            for ot in [1, 2]:
                fixture_info_df['team{}_{}'.format(ot, info)] = [
                    v.get('side{}'.format(ot), {}).get(info, np.nan)
                    for v in fixture_values]
        for ou_i, ou in [(1, 'over'), (2, 'under')]:
            for info in ['spread', 'price']:
                fixture_info_df['totals_{}_{}'.format(ou, info)] = [
                    v.get('side{}'.format(ou_i), {}).get(
                        'totals_{}'.format(info), np.nan)
                    for v in fixture_values]

        # convert some columns to numeric
        for col in self._cols_to_numeric:
            fixture_info_df[col] = fixture_info_df[col].astype(float)

        return fixture_info_df

    def _write_to_db(self, fixture_info_df, db_type='mongo'):
        if db_type == 'mongo':
            from pymongo import MongoClient

            db_client = MongoClient()

            db_ = db_client.sport[self.sport].pinnacle_odds
            if self.sport == 'icehockey':
                db_ = db_.overtime_included

            for r in fixture_info_df.to_dict(orient='records'):
                db_.update(r, r, upsert=True)

        elif db_type == 'mysql':
            # TODO: test this section on pythonanywhere
            import mysql.connector

            mydb = mysql.connector.connect(
                host="PatrickS.mysql.pythonanywhere-services.com",
                user="PatrickS",
                passwd="datadata",
                database="PatrickS$pinnacle_odds"
            )

            mycursor = mydb.cursor()
            mycursor.execute("SHOW TABLES")
            mytables = [t[0] for t in mycursor.fetchall()]
            if self.sport not in mytables:
                print 'creating table'
                table_ = ('CREATE TABLE {} ('
                          'fixture_date VARCHAR(255), comp VARCHAR(255), '
                          'team1_name VARCHAR(255), team2_name VARCHAR(255), '
                          'team1_moneyline FLOAT, team2_moneyline FLOAT, '
                          'team1_handicap_spread VARCHAR(50), team2_handicap_spread VARCHAR(50), '
                          'team1_handicap_price FLOAT, team2_handicap_price FLOAT, '
                          'totals_over_spread VARCHAR(50), totals_over_price FLOAT, '
                          'totals_under_spread VARCHAR(50), totals_under_price FLOAT, '
                          'datetime DATETIME'
                          .format(self.sport))
                mycursor.execute(table_)

            for r in fixture_info_df.to_dict(orient='records'):
                try:
                    sql = ('INSERT INTO {} (fixture_date, comp, '
                           'team1_name, team2_name, team1_moneyline, team2_moneyline, '
                           'team1_handicap_spread, team2_handicap_spread, '
                           'team1_handicap_price, team2_handicap_price, '
                           'totals_over_spread, totals_over_price, '
                           'totals_under_spread, totals_under_price, '
                           'datetime) '
                           'VALUES (%s, %s, %s, %s, %s, %s, %s, %s, '
                           '%s, %s, %s, %s, %s, %s, %s)'
                           .format(self.sport))
                    val = (r['fixture_date'], r['comp'],
                           r['team1_name'], r['team2_name'],
                           r['team1_moneyline'], r['team2_moneyline'],
                           r['team1_handicap_spread'], r['team2_handicap_spread'],
                           r['team1_handicap_price'], r['team2_handicap_price'],
                           r['totals_over_spread'], r['totals_over_price'],
                           r['totals_under_spread'], r['totals_under_price'],
                           r['datetime'])
                    mycursor.execute(sql, val)
                    mydb.commit()
                    print 'wrote {}'.format(r)
                except:
                    mydb.rollback()

            mydb.close()

        else:
            raise ValueError('unrecognised db_type {}'.format(db_type))
