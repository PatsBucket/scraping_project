#!/usr/bin/python

__author__ = 'psessford'


def get_postgres_credentials(db):
    if db == 'primary':
        host = '94.229.79.74'
    elif db == 'replica':
        host = '5.101.138.210'
    else:
        raise ValueError('unrecognised db {} (expected primary or replica)'
                         .format(db))

    conn_string = (
        "host='{host}' "
        "dbname='football' "
        "user='psessford' "
        "password='datadataeverywhere'"
        .format(host=host)
    )
    return conn_string


def get_table_names(db_cursor):
    db_cursor.execute("""
        SELECT table_name
          FROM information_schema.tables
         WHERE table_schema='whoscored'
           AND table_type='BASE TABLE';
    """)
    tables = [t[0] for t in db_cursor.fetchall()]
    return tables
