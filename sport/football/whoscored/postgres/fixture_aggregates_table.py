#!/usr/bin/python

import psycopg2
import psycopg2.extras
from sport.football.whoscored.postgres.common import (get_postgres_credentials,
                                                      get_table_names)
from sport.football.whoscored.postgres.raw_table import (
    get_fixtures_raw_data_from_db)

__author__ = 'psessford'


def create_fixture_aggregates_postgres_table():
    """Creates Postgres table for aggregated fixture data from raw data
    """
    db_client = psycopg2.connect(get_postgres_credentials(db='primary'))
    db_cursor = db_client.cursor()

    tables = get_table_names(db_cursor=db_cursor)
    if 'fixture_aggregates' in tables:
        raise Exception('fixture_aggregates table already exists')
    else:
        db_cursor.execute("""
            CREATE TABLE whoscored.fixture_aggregates (
                         fixture_id INT, 
                         team1_total_shots INT, 
                         team2_total_shots INT, 
                         team1_possession DOUBLE PRECISION, 
                         team2_possession DOUBLE PRECISION, 
                         team1_pass_success_pct INT, 
                         team2_pass_success_pct INT, 
                         team1_dribbles INT, 
                         team2_dribbles INT, 
                         team1_aerials_won INT, 
                         team2_aerials_won INT, 
                         team1_successful_tackles INT, 
                         team2_successful_tackles INT, 
                         team1_corners INT, 
                         team2_corners INT, 
                         team1_dispossessed INT, 
                         team2_dispossessed INT, 
                         team1_total_saves INT, 
                         team2_total_saves INT
                         )
        """)

    db_cursor.close()
    try:
        db_client.commit()
    except:
        db_client.rollback()

    db_client.close()


def get_fixture_aggregates_from_db(fixture_id):
    """Get aggregated data from data base for a single fixture

    :param fixture_id: (int)
    :return: (dict)
    """
    db_client = psycopg2.connect(get_postgres_credentials(db='replica'))
    db_cursor = db_client.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    db_cursor.execute("""
        SELECT * FROM whoscored.fixture_aggregates
         WHERE whoscored.fixture_aggregates.fixture_id = {}
    """.format(fixture_id))
    db_fixture_aggs = db_cursor.fetchall()

    if len(db_fixture_aggs) == 0:
        db_fixture_aggs = None
    else:
        assert (len(db_fixture_aggs) == 1)
        db_fixture_aggs = db_fixture_aggs[0]  # single fixture

    db_cursor.close()
    db_client.close()
    return db_fixture_aggs


def get_fixtures_aggregates_from_db(fixture_ids):
    """Get aggregated fixture data info from data base for multiple fixtures

    :param fixture_ids: (list of int) if 'all', will return all fixtures in db
    :return db_fixtures_data: (list of dict)
    """
    db_client = psycopg2.connect(get_postgres_credentials(db='replica'))
    db_cursor = db_client.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    sql = ("""
        SELECT * FROM whoscored.fixture_aggregates
    """)

    if isinstance(fixture_ids, list):
        sql += ("""
            WHERE whoscored.fixture_aggregates.fixture_id IN {}
        """.format(tuple(fixture_ids)))
    else:
        assert (fixture_ids == 'all')

    db_cursor.execute(sql)
    db_fixtures_data = db_cursor.fetchall()

    db_cursor.close()
    db_client.close()
    return db_fixtures_data


def populate_fixture_aggregates_table(fixture_ids,
                                      method='fixture_by_fixture'):
    """Use raw (unstructured) data from data base to create a table for
    aggregated fixture data

    :param fixture_ids: (list of int) if 'all', will do all fixtures in raw db
    :param method: (str) e.g. 'fixture_by_fixture' when inserting
    """
    db_client = psycopg2.connect(get_postgres_credentials(db='primary'))
    db_cursor = db_client.cursor()

    raw_data = get_fixtures_raw_data_from_db(
        fixture_ids=fixture_ids, db_cursor=db_cursor, close_cursor=False)

    if method == 'fixture_by_fixture':
        for f_id, f_data in raw_data.iteritems():
            db_fixture_aggs = get_fixture_aggregates_from_db(fixture_id=f_id)
            if db_fixture_aggs:
                continue  # fixture already in table

            d_agg = f_data.get('aggregated_match_data', {})

            vals = {
                'fixture_id': f_id,
                'team1_total_shots': d_agg.get('shotsTotal', {}).get('home'),
                'team2_total_shots': d_agg.get('shotsTotal', {}).get('away'),
                'team1_possession': d_agg.get('possession', {}).get('home'),
                'team2_possession': d_agg.get('possession', {}).get('away'),
                'team1_pass_success_pct': d_agg.get('passSuccess', {}).get(
                    'home'),
                'team2_pass_success_pct': d_agg.get('passSuccess', {}).get(
                    'away'),
                'team1_dribbles': d_agg.get('dribblesWon', {}).get('home'),
                'team2_dribbles': d_agg.get('dribblesWon', {}).get('away'),
                'team1_aerials_won': d_agg.get('aerialsWon', {}).get('home'),
                'team2_aerials_won': d_agg.get('aerialsWon', {}).get('away'),
                'team1_successful_tackles': d_agg.get(
                    'tackleSuccessful', {}).get('home'),
                'team2_successful_tackles': d_agg.get(
                    'tackleSuccessful', {}).get('away'),
                'team1_corners': d_agg.get('cornersTotal', {}).get('home'),
                'team2_corners': d_agg.get('cornersTotal', {}).get('away'),
                'team1_dispossessed': d_agg.get('dispossessed', {}).get(
                    'home'),
                'team2_dispossessed': d_agg.get('dispossessed', {}).get(
                    'away'),
                'team1_total_saves': d_agg.get('totalSaves', {}).get('home'),
                'team2_total_saves': d_agg.get('totalSaves', {}).get('away'),
            }

            sql = ("""
                INSERT INTO whoscored.fixture_aggregates (
                                 fixture_id, 
                                 team1_total_shots, 
                                 team2_total_shots, 
                                 team1_possession, 
                                 team2_possession, 
                                 team1_pass_success_pct, 
                                 team2_pass_success_pct, 
                                 team1_dribbles, 
                                 team2_dribbles, 
                                 team1_aerials_won, 
                                 team2_aerials_won, 
                                 team1_successful_tackles, 
                                 team2_successful_tackles, 
                                 team1_corners, 
                                 team2_corners, 
                                 team1_dispossessed, 
                                 team2_dispossessed, 
                                 team1_total_saves, 
                                 team2_total_saves
                                 )
                VALUES (
                       %(fixture_id)s, 
                       %(team1_total_shots)s, 
                       %(team2_total_shots)s, 
                       %(team1_possession)s, 
                       %(team2_possession)s, 
                       %(team1_pass_success_pct)s, 
                       %(team2_pass_success_pct)s, 
                       %(team1_dribbles)s, 
                       %(team2_dribbles)s, 
                       %(team1_aerials_won)s, 
                       %(team2_aerials_won)s, 
                       %(team1_successful_tackles)s, 
                       %(team2_successful_tackles)s, 
                       %(team1_corners)s, 
                       %(team2_corners)s, 
                       %(team1_dispossessed)s, 
                       %(team2_dispossessed)s, 
                       %(team1_total_saves)s, 
                       %(team2_total_saves)s
                       )
            """)
            db_cursor.execute(sql, vals)

    else:
        NotImplementedError('inserting all at once not currently implemented')

    db_cursor.close()
    try:
        db_client.commit()
    except:
        db_client.rollback()

    db_client.close()


if __name__ == '__main__':
    f_ids = [1317601, 1284899]

    # create_fixture_aggregates_postgres_table()
    # populate_fixture_aggregates_table(fixture_ids=f_ids)

    data = get_fixtures_aggregates_from_db(fixture_ids=f_ids)

    pass
