#!/usr/bin/python

from datetime import datetime
import psycopg2
import psycopg2.extras
from sport.football.whoscored.postgres.common import (get_postgres_credentials,
                                                      get_table_names)
from sport.football.whoscored.postgres.raw_table import (
    get_fixtures_raw_data_from_db)

__author__ = 'psessford'


def create_fixture_info_postgres_table():
    """Creates Postgres table for fixture information from raw data
    """
    db_client = psycopg2.connect(get_postgres_credentials(db='primary'))
    db_cursor = db_client.cursor()

    tables = get_table_names(db_cursor=db_cursor)
    if 'fixture_info' in tables:
        raise Exception('fixture_info table already exists')
    else:
        db_cursor.execute("""
            CREATE TABLE whoscored.fixture_info (
                         fixture_id INT, 
                         kick_off TIMESTAMP,
                         region VARCHAR(255), 
                         competition VARCHAR(255), 
                         team1_name VARCHAR(255), 
                         team2_name VARCHAR(255), 
                         team1_id INT, 
                         team2_id INT, 
                         team1_ht_score INT,
                         team2_ht_score INT,
                         team1_ft_score INT,
                         team2_ft_score INT,
                         team1_manager_name VARCHAR(255),
                         team2_manager_name VARCHAR(255),
                         referee_name VARCHAR(255),
                         referee_id INT,
                         attendance INT
                         )
        """)

    db_cursor.close()
    try:
        db_client.commit()
    except:
        db_client.rollback()

    db_client.close()


def get_fixture_info_from_db(fixture_id):
    """Get fixture info from data base for a single fixture

    :param fixture_id: (int)
    :return: (dict)
    """
    db_client = psycopg2.connect(get_postgres_credentials(db='replica'))
    db_cursor = db_client.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    db_cursor.execute("""
        SELECT * FROM whoscored.fixture_info
         WHERE whoscored.fixture_info.fixture_id = {}
    """.format(fixture_id))
    db_fixture_info = db_cursor.fetchall()

    if len(db_fixture_info) == 0:
        db_fixture_info = None
    else:
        assert (len(db_fixture_info) == 1)
        db_fixture_info = db_fixture_info[0]  # single fixture

    db_cursor.close()
    db_client.close()
    return db_fixture_info


def get_fixtures_info_from_db(fixture_ids):
    """Get fixture info from data base for multiple fixtures

    :param fixture_ids: (list of int) if 'all', will return all fixtures in db
    :return db_fixtures_data: (list of dict)
    """
    db_client = psycopg2.connect(get_postgres_credentials(db='replica'))
    db_cursor = db_client.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    sql = ("""
        SELECT * FROM whoscored.fixture_info
    """)

    if isinstance(fixture_ids, list):
        sql += ("""
            WHERE whoscored.fixture_info.fixture_id IN {}
        """.format(tuple(fixture_ids)))
    else:
        assert (fixture_ids == 'all')

    db_cursor.execute(sql)
    db_fixtures_data = db_cursor.fetchall()

    db_cursor.close()
    db_client.close()
    return db_fixtures_data


def populate_fixture_info_table(fixture_ids, method='fixture_by_fixture'):
    """Use raw (unstructured) data from data base to create a table for basic
    fixture information

    :param fixture_ids: (list of int) if 'all', will do all fixtures in raw db
    :param method: (str) e.g. 'fixture_by_fixture' when inserting
    """
    db_client = psycopg2.connect(get_postgres_credentials(db='primary'))
    db_cursor = db_client.cursor()

    raw_data = get_fixtures_raw_data_from_db(
        fixture_ids=fixture_ids, db_cursor=db_cursor, close_cursor=False)

    if method == 'fixture_by_fixture':
        for f_id, f_data in raw_data.iteritems():
            db_fixture_info = get_fixture_info_from_db(fixture_id=f_id)
            if db_fixture_info:
                continue  # fixture already in table

            f_start_time = f_data.get('full_match_data', {}).get('startTime')
            if isinstance(f_start_time, basestring):
                # f_start_time = f_start_time.replace('T', ' ')
                f_start_time = datetime.strptime(f_start_time,
                                                 '%Y-%m-%dT%H:%M:%S')

            d_team1_full = f_data.get('full_match_data', {}).get('home', {})
            d_team2_full = f_data.get('full_match_data', {}).get('away', {})
            d_ref_full = f_data.get('full_match_data', {}).get('referee', {})

            str_vals = {
                'region': f_data.get('competition', {}).get('region'),
                'competition': f_data.get('competition', {}).get(
                    'competition'),
                'team1_name': d_team1_full.get('name'),
                'team1_manager_name': d_team1_full.get('managerName'),
                'team2_name': d_team2_full.get('name'),
                'team2_manager_name': d_team2_full.get('managerName'),
                'referee_name': d_ref_full.get('name'),
            }
            for k, v in str_vals.iteritems():
                if v is not None:
                    str_vals[k] = v.encode('utf-8').strip()  # overwrite

            vals = str_vals.copy()
            vals.update({
                'fixture_id': f_id,
                'kick_off': f_start_time,
                'team1_id': d_team1_full.get('teamId'),
                'team2_id': d_team2_full.get('teamId'),
                'team1_ht_score': d_team1_full.get('scores', {}).get(
                    'halftime'),
                'team2_ht_score': d_team2_full.get('scores', {}).get(
                    'halftime'),
                'team1_ft_score': d_team1_full.get('scores', {}).get(
                    'fulltime'),
                'team2_ft_score': d_team2_full.get('scores', {}).get(
                    'fulltime'),
                'referee_id': d_ref_full.get('officialId'),
                'attendance': f_data.get('full_match_data', {}).get(
                    'attendance'),
            })

            sql = ("""
                INSERT INTO whoscored.fixture_info (
                                 fixture_id, 
                                 kick_off,
                                 region,
                                 competition,
                                 team1_name, 
                                 team2_name, 
                                 team1_id, 
                                 team2_id, 
                                 team1_ht_score,
                                 team2_ht_score,
                                 team1_ft_score,
                                 team2_ft_score,
                                 team1_manager_name,
                                 team2_manager_name,
                                 referee_name,
                                 referee_id,
                                 attendance
                                 )
                VALUES (
                       %(fixture_id)s, 
                       %(kick_off)s, 
                       %(region)s, 
                       %(competition)s, 
                       %(team1_name)s, 
                       %(team2_name)s, 
                       %(team1_id)s, 
                       %(team2_id)s, 
                       %(team1_ht_score)s, 
                       %(team2_ht_score)s, 
                       %(team1_ft_score)s, 
                       %(team2_ft_score)s, 
                       %(team1_manager_name)s, 
                       %(team2_manager_name)s, 
                       %(referee_name)s, 
                       %(referee_id)s, 
                       %(attendance)s
                       )
            """)
            db_cursor.execute(sql, vals)

    else:
        NotImplementedError('inserting all at once not currently implemented')

    db_cursor.close()
    try:
        db_client.commit()
    except:
        db_client.rollback()

    db_client.close()


if __name__ == '__main__':
    f_ids = [1317601, 1284899]

    # create_fixture_info_postgres_table()
    # populate_fixture_info_table(fixture_ids=f_ids)

    data = get_fixtures_info_from_db(fixture_ids=f_ids)

    pass
