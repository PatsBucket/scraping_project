#!/usr/bin/python

import psycopg2
import psycopg2.extras
from sport.football.whoscored.postgres.common import (get_postgres_credentials,
                                                      get_table_names)
from sport.football.whoscored.postgres.raw_table import (
    get_fixtures_raw_data_from_db)

__author__ = 'psessford'


def create_play_by_play_postgres_table():
    """Creates Postgres table for play-by-play data from raw data
    """
    db_client = psycopg2.connect(get_postgres_credentials(db='primary'))
    db_cursor = db_client.cursor()

    tables = get_table_names(db_cursor=db_cursor)
    if 'play_by_play' in tables:
        raise Exception('play_by_play table already exists')
    else:
        db_cursor.execute("""
            CREATE TABLE whoscored.play_by_play (
                         fixture_id INT, 
                         event_id INT,
                         team_id INT,
                         player_id INT,
                         period_display_name VARCHAR(255),
                         period_value INT,
                         match_minute INT,
                         match_second INT, 
                         expanded_minute INT,
                         coord_x DOUBLE PRECISION, 
                         coord_y DOUBLE PRECISION, 
                         is_touch BOOLEAN, 
                         type_display_name VARCHAR(255),
                         type_display_value INT,
                         outcome_type_name VARCHAR(255),
                         outcome_type_value INT,
                         id DOUBLE PRECISION,
                         satisfied_events_types VARCHAR(255),
                         satisfied_events_type_names VARCHAR(255)
                         )
        """)

    db_cursor.close()
    try:
        db_client.commit()
    except:
        db_client.rollback()

    db_client.close()


def get_fixture_play_by_play_from_db(fixture_id, limit=None):
    """Get play-by-play data from data base for a single fixture

    :param fixture_id: (int)
    :param limit: (None or int) limit the number of rows returned
    :return: (dict)
    """
    db_client = psycopg2.connect(get_postgres_credentials(db='replica'))
    db_cursor = db_client.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    sql = ("""
        SELECT * FROM whoscored.play_by_play
         WHERE whoscored.play_by_play.fixture_id = {}
    """.format(fixture_id))

    if limit is not None:
        sql += ("""
            LIMIT {}
        """.format(limit))

    db_cursor.execute(sql)
    db_play_by_play = db_cursor.fetchall()

    if len(db_play_by_play) == 0:
        db_play_by_play = None

    db_cursor.close()
    db_client.close()
    return db_play_by_play


def get_fixtures_play_by_play_from_db(fixture_ids):
    """Get play-by-play data from data base for multiple fixtures

    :param fixture_ids: (list of int) if 'all', will return all fixtures in db
    :return db_fixtures_data: (list of dict)
    """
    db_client = psycopg2.connect(get_postgres_credentials(db='replica'))
    db_cursor = db_client.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    sql = ("""
        SELECT * FROM whoscored.play_by_play
    """)
    # TODO: add argument to select a subset of columns?

    if isinstance(fixture_ids, list):
        sql += ("""
            WHERE whoscored.play_by_play.fixture_id IN {}
        """.format(tuple(fixture_ids)))
    else:
        assert (fixture_ids == 'all')

    db_cursor.execute(sql)
    db_fixtures_data = db_cursor.fetchall()

    db_cursor.close()
    db_client.close()
    return db_fixtures_data


def populate_play_by_play_table(fixture_ids, method='fixture_by_fixture'):
    """Use raw (unstructured) data from data base to create a table for
    play-by-play data

    :param fixture_ids: (list of int) if 'all', will do all fixtures in raw db
    :param method: (str) e.g. 'fixture_by_fixture' when inserting
    """
    db_client = psycopg2.connect(get_postgres_credentials(db='primary'))
    db_cursor = db_client.cursor()

    raw_data = get_fixtures_raw_data_from_db(
        fixture_ids=fixture_ids, db_cursor=db_cursor, close_cursor=False)

    if method == 'fixture_by_fixture':
        # TODO: consider using db_cursor.insertmany() and a list of insert vals
        for f_id, f_data in raw_data.iteritems():
            db_play_by_play = get_fixture_play_by_play_from_db(fixture_id=f_id,
                                                               limit=1)
            if db_play_by_play:
                continue  # fixture already in table

            evt_key = {v: k for k, v in f_data['event_key'].iteritems()}

            for evt in f_data.get('full_match_data', {}).get('events', []):
                str_vals = {
                    'satisfied_events_types': '_'.join(
                        [str(e) for e in evt.get(
                            'satisfiedEventsTypes', [])]),
                    'satisfied_events_type_names': '__'.join([evt_key.get(
                        e, 'unknown') for e in evt.get(
                        'satisfiedEventsTypes', [])]),
                    'period_display_name': evt.get('period', {}).get(
                        'displayName'),
                    'type_display_name': evt.get('type', {}).get(
                        'displayName'),
                    'outcome_type_name': evt.get('outcomeType', {}).get(
                        'displayName'),
                }
                for k, v in str_vals.iteritems():
                    if v is not None:
                        str_vals[k] = v.encode('utf-8').strip()  # overwrite

                vals = str_vals.copy()
                vals.update({
                    'fixture_id': f_id,
                    'event_id': evt.get('eventId'),
                    'team_id': evt.get('teamId'),
                    'player_id': evt.get('playerId'),
                    'period_value': evt.get('period', {}).get('value'),
                    'match_minute': evt.get('minute'),
                    'match_second': evt.get('second'),
                    'expanded_minute': evt.get('expandedMinute'),
                    'coord_x': evt.get('x'),
                    'coord_y': evt.get('y'),
                    'is_touch': evt.get('isTouch'),
                    'type_display_value': evt.get('type', {}).get('value'),
                    'outcome_type_value': evt.get('outcomeType', {}).get(
                        'value'),
                    'id': evt.get('id'),
                })

                sql = ("""
                    INSERT INTO whoscored.play_by_play (
                                     fixture_id, 
                                     event_id, 
                                     team_id, 
                                     player_id, 
                                     period_display_name, 
                                     period_value, 
                                     match_minute, 
                                     match_second, 
                                     expanded_minute, 
                                     coord_x, 
                                     coord_y, 
                                     is_touch, 
                                     type_display_name, 
                                     type_display_value, 
                                     outcome_type_name, 
                                     outcome_type_value, 
                                     id, 
                                     satisfied_events_types, 
                                     satisfied_events_type_names
                                     )
                    VALUES (
                         %(fixture_id)s, 
                         %(event_id)s, 
                         %(team_id)s, 
                         %(player_id)s, 
                         %(period_display_name)s, 
                         %(period_value)s, 
                         %(match_minute)s, 
                         %(match_second)s, 
                         %(expanded_minute)s, 
                         %(coord_x)s, 
                         %(coord_y)s, 
                         %(is_touch)s, 
                         %(type_display_name)s, 
                         %(type_display_value)s, 
                         %(outcome_type_name)s, 
                         %(outcome_type_value)s, 
                         %(id)s, 
                         %(satisfied_events_types)s, 
                         %(satisfied_events_type_names)s
                         )
                """)
                db_cursor.execute(sql, vals)
                # TODO: also use the data on 'qualifiers'?

    else:
        NotImplementedError('inserting all at once not currently implemented')

    db_cursor.close()
    try:
        db_client.commit()
    except:
        db_client.rollback()

    db_client.close()


if __name__ == '__main__':
    f_ids = [1317601, 1284899]

    create_play_by_play_postgres_table()
    populate_play_by_play_table(fixture_ids=f_ids)

    # data = get_fixtures_play_by_play_from_db(fixture_ids=f_ids)

    pass
