#!/usr/bin/python

import json
import psycopg2
import psycopg2.extras
from sport.football.whoscored.postgres.common import (get_postgres_credentials,
                                                      get_table_names)

__author__ = 'psessford'


def check_postgres_raw_table(db_cursor):
    """Creates table for raw (unstructured) data if it's not already created

    :param db_cursor: (psycopg2.extensions.cursor)
    :return: (bool) whether table was found
    """
    tables = get_table_names(db_cursor=db_cursor)

    if 'raw' in tables:
        db_cursor.close()
        return True  # no need to create table
    else:
        db_cursor.execute(  # create table
            'CREATE TABLE whoscored.raw (fixture_id INT, raw_data JSONB)'
        )
        db_cursor.close()
        return False


def get_fixture_raw_data_from_db(fixture_id, db_cursor=None):
    """Get raw (unstructured) data from data base for a single fixture

    :param fixture_id: (int)
    :param db_cursor: (psycopg2.extensions.cursor) if None, will be created
    :return: (dict)
    """
    if db_cursor is None:
        db_client = psycopg2.connect(get_postgres_credentials(db='replica'))
        db_cursor = db_client.cursor()

    db_cursor.execute("""
        SELECT raw_data FROM whoscored.raw
         WHERE whoscored.raw.fixture_id = {}
    """.format(fixture_id))
    db_fixture_data = db_cursor.fetchall()

    if len(db_fixture_data) == 0:
        db_fixture_data = None
    else:
        for _ in [0, 1]:  # do twice
            assert (len(db_fixture_data) == 1)
            db_fixture_data = db_fixture_data[0]  # single fixture; single col

    db_cursor.close()
    if db_cursor is None:
        db_client.close()

    return db_fixture_data


def get_fixtures_raw_data_from_db(fixture_ids, db_cursor=None,
                                  close_cursor=True):
    """Get raw (unstructured) data from data base for multiple fixtures

    :param fixture_ids: (list of int) if 'all', will return all fixtures in db
    :param db_cursor: (psycopg2.extensions.cursor) if None, will be created
    :param close_cursor: (bool)
    :return: (dict) keys are fixture ids, values are dicts of raw data
    """
    if db_cursor is None:
        db_client = psycopg2.connect(get_postgres_credentials(db='replica'))
        db_cursor = db_client.cursor()

    sql = ("""
        SELECT fixture_id, raw_data FROM whoscored.raw
    """)

    if isinstance(fixture_ids, list):
        sql += ("""
            WHERE whoscored.raw.fixture_id IN {}
        """.format(tuple(fixture_ids)))
    else:
        assert (fixture_ids == 'all')

    db_cursor.execute(sql)
    db_fixture_data = db_cursor.fetchall()
    db_fixture_data = {d[0]: d[1] for d in db_fixture_data}

    if close_cursor:
        db_cursor.close()

    if db_cursor is None:
        db_client.close()

    return db_fixture_data


def write_fixture_raw_data_to_db(db_cursor, fixture_id, data_dict):
    """Write raw (unstructured) data to data base

    :param db_cursor: (psycopg2.extensions.cursor)
    :param fixture_id: (int)
    :param data_dict: (dict)
    """
    sql = ("""
        INSERT INTO whoscored.raw (fixture_id, raw_data) 
        VALUES (%s, %s)
    """)
    vals = (fixture_id, json.dumps(data_dict))
    db_cursor.execute(sql, vals)
    db_cursor.close()


if __name__ == '__main__':
    f_ids = [1317601, 1284899]
    data = get_fixtures_raw_data_from_db(fixture_ids=f_ids)
    pass
