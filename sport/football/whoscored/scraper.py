#!/usr/bin/python

import logging
from datetime import datetime, timedelta
import pytz
from bs4 import BeautifulSoup
import json
import time
from pymongo import MongoClient
import psycopg2
from sport.football.whoscored.comp_and_season_url import (
    get_comp_and_season_url)
from sport.football.whoscored.postgres.raw_table import (
    check_postgres_raw_table, get_fixture_raw_data_from_db,
    write_fixture_raw_data_to_db)
from sport.football.whoscored.postgres.common import get_postgres_credentials

__author__ = 'psessford'


class WhoScoredScraper(object):
    """Helper to scrape football data from WhoScored.com
    """
    def __init__(self, db_type='mongo'):
        self._db_type = db_type
        if db_type not in ['mongo', 'postgres']:
            raise ValueError('unrecognised db_type {}'.format(db_type))

        # helpers for a properties
        self._db_client = None
        self._db_client_replica = None

        logging.basicConfig(level=logging.INFO)
        self.logger = logging.getLogger(__name__)
        self.logger.info('initialising a WhoScoredScraper instance')

    @property
    def db_connection(self):
        if self._db_client is None:
            self.logger.info('opening data base client')
            if self._db_type == 'mongo':
                self._db_client = MongoClient()
            elif self._db_type == 'postgres':
                self._db_client = psycopg2.connect(get_postgres_credentials(
                    db='primary'))
                db_cursor = self._db_client.cursor()
                if not check_postgres_raw_table(db_cursor=db_cursor):
                    self._db_client.commit()
                    self.logger.info('table created in data base')
            else:
                raise ValueError('unrecognised db_type {}'
                                 .format(self._db_type))

        if self._db_type == 'mongo':
            return self._db_client.sport.football.whoscored.raw_data
        elif self._db_type == 'postgres':
            return self._db_client
        else:
            raise ValueError('unrecognised db_type {}'.format(self._db_type))

    @property
    def replica_db_connection(self):
        assert (self._db_type == 'postgres')  # otherwise not relevant
        if self._db_client_replica is None:
            self._db_client_replica = psycopg2.connect(
                get_postgres_credentials(db='replica'))

        return self._db_client_replica

    def close_db_connection(self):
        self.logger.info('closing data base client')
        self._db_client.close()
        if self._db_client_replica is not None:
            self._db_client_replica.close()

    def get_fixture_ids(self, comp, season):
        """Get all fixture ids for all a comp and season

        :param comp: (str) e.g. 'EngPr'
        :param season: (str) e.g. '2018/2019'

        :return fixture_ids: (list of int)
        """
        self.logger.info('getting fixture ids for comp {} season {}'
                         .format(comp, season))
        url = get_comp_and_season_url(comp=comp, season=season)
        client_response, now = self._get_client_response(url=url,
                                                         method='selenium')
        time.sleep(2)

        self._check_competition_stages(url=url,
                                       client_response=client_response)

        fixture_ids = []  # initialise
        new_fixtures_found = True
        while new_fixtures_found:
            fixture_ids, new_fixtures_found = self._add_to_fixtures_ids(
                fixture_ids=fixture_ids, new_fixtures_found=new_fixtures_found,
                client_response=client_response)

        client_response.close()
        fixture_ids = [int(f_id) for f_id in fixture_ids]
        fixture_ids.sort()
        return fixture_ids

    def _check_competition_stages(self, url, client_response):
        if 'Stages/' not in url:  # i.e. if stage is not specified
            self.logger.info("checking for a 'stages' button due to the stage "
                             "not being in the specified url")
            try:
                client_response.find_element_by_id('stages')
                self.logger.info("'stages' button found for competition")
                stages_button_found = True
            except:
                self.logger.info("no 'stages' button found for competition")
                stages_button_found = False

            if stages_button_found:
                raise Exception('if competition has multiple stages then the '
                                'stage should be specified in the url '
                                '(please consider updating the url)')

    @staticmethod
    def scrape_fixtures(fixture_ids, method='selenium', write_to_db=False,
                        n_days_safe=5, db_type='mongo'):
        # TODO: make this a class method?
        """Do scraping for specified fixture ids

        :param fixture_ids: (list of int) that single int also accepted
        :param method: (str) library to use for the scraping
        :param write_to_db: (bool) whether to write to data base
        :param n_days_safe: (int) number of days after kick off to scrape
        :param db_type: (str) whether to use mongo or postgress data base

        :return data: (dict) keys are fixture ids
        """
        if not isinstance(fixture_ids, list):
            fixture_ids = [fixture_ids]

        ws_scapper = WhoScoredScraper(db_type=db_type)

        data = {}
        for fixture_id in fixture_ids:
            data[fixture_id] = ws_scapper._scrape_single_fixture(
                fixture_id=fixture_id, method=method, write_to_db=write_to_db,
                n_days_safe=n_days_safe)

        if write_to_db:
            ws_scapper.close_db_connection()

        return data

    def _add_to_fixtures_ids(self, fixture_ids, new_fixtures_found,
                             client_response):
        from selenium.webdriver.common.keys import Keys

        source_html = self._get_source_html(
            client_response=client_response, method='selenium',
            close_client=False)

        soup = BeautifulSoup(source_html, 'lxml')
        # soup.find_all('a', class_='match-link match-report rc', href=True)
        f_ids = soup.find_all('tr', attrs={'data-id': True})
        f_ids = [f_id['data-id'] for f_id in f_ids]
        n_f_ids = len(list(set(f_ids)))
        self.logger.info('{} fixtures found'.format(n_f_ids))

        n_total_fixtures_old = len(fixture_ids)
        fixture_ids += f_ids
        fixture_ids = list(set(fixture_ids))  # remove any duplicates
        n_total_fixtures_new = len(fixture_ids)
        self.logger.info('total number of fixtures is {}'
                         .format(n_total_fixtures_new))
        if n_total_fixtures_new == n_total_fixtures_old:
            new_fixtures_found = False

        client_response.find_element_by_class_name('previous').send_keys(
            Keys.RETURN)
        time.sleep(2)

        return fixture_ids, new_fixtures_found

    def _scrape_single_fixture(self, fixture_id, method, write_to_db,
                               n_days_safe):
        msg = ('looking to scrape data for fixture {} (with write_to_db {} '
               'and n_days_safe {})'
               .format(fixture_id, write_to_db, n_days_safe))
        self.logger.info(msg)

        if write_to_db:
            if self._is_fixture_in_db(fixture_id=fixture_id):
                return None

        url = ('https://www.whoscored.com/Matches/{}/Live/'.format(fixture_id))
        client_response, now = self._get_client_response(url=url,
                                                         method=method)
        source_html = self._get_source_html(client_response=client_response,
                                            method=method, close_client=True)
        time.sleep(3)  # ensure web site is not hit rapidly

        if 'The page you requested does not exist' in source_html:
            self.logger.info('no web page found')
            return None

        self.logger.info('parsing web page html into BeautifulSoup object')
        soup = BeautifulSoup(source_html, 'lxml')
        # with open("tmp_whoscored", "wb") as f:
        #     f.write(soup.prettify().encode("UTF-8"))

        comp_info_dict = self._get_comp_info_from_soup(soup)
        match_stats_dict = self._get_match_stats_from_soup(soup=soup)

        scripts = soup.find_all('script')
        data_dict = {}  # initialise
        for label, var in [('full_match_data', 'matchCentreData'),
                           ('event_key', 'matchCentreEventTypeJson'),
                           ('fixture_id', 'matchId'),
                           ('formation_key', 'formationIdNameMappings')]:
            data_dict[label] = self._extract_var_from_scripts(var=var,
                                                              scripts=scripts)

        assert (data_dict['fixture_id'] == fixture_id)
        data_dict['competition'] = comp_info_dict
        data_dict['aggregated_match_data'] = match_stats_dict
        # data_dict['datetime'] = now

        if any([d is None for d in data_dict.values()]):
            self.logger.info('incomplete data; returning None')
            return None

        if 'startTime' not in data_dict['full_match_data']:
            self.logger.info('startTime not found in data; returning None')
            return None

        start_time = data_dict['full_match_data']['startTime']
        try:
            start_time = datetime.strptime(start_time, '%Y-%m-%dT%H:%M:%S')
            start_time = pytz.utc.localize(start_time)
        except:
            self.logger.info('startTime could not be parsed; returning None')
            return None

        if (start_time + timedelta(days=n_days_safe)) >= now:
            self.logger.info('startTime too recent; returning None')
            return None

        if write_to_db:
            self._write_to_db(data_dict=data_dict, fixture_id=fixture_id)

        self.logger.info('returning data for fixture {}'.format(fixture_id))
        return data_dict

    def _is_fixture_in_db(self, fixture_id):
        if self._db_type == 'mongo':
            db_fixture_data = self.db_connection.find_one(
                {'fixture_id': fixture_id})
        elif self._db_type == 'postgres':
            db_fixture_data = get_fixture_raw_data_from_db(
                db_cursor=self.replica_db_connection.cursor(),
                fixture_id=fixture_id)
        else:
            raise ValueError('unrecognised db_type {}'.format(self._db_type))

        if db_fixture_data:
            self.logger.info('data already found in data base')
            return True
        else:
            return False

    def _get_client_response(self, url, method):
        self.logger.info('getting web page response (using method {})'
                         .format(method))
        if method == 'dynamic_client':
            from dynamic_client import DynamicClient

            client_response = DynamicClient(url=url)
            now = datetime.now(tz=pytz.utc)

        elif method == 'selenium':
            from selenium import webdriver
            from selenium.webdriver.chrome.options import Options
            from webdriver_manager.chrome import ChromeDriverManager

            chrome_options = Options()
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--window-size=1920x1080')
            executable_path = ChromeDriverManager().install()

            client_response_success = False
            for _ in range(5):  # retry if connection to url fails
                try:
                    client_response = webdriver.Chrome(
                        chrome_options=chrome_options,
                        executable_path=executable_path)
                    client_response.get(url=url)
                    now = datetime.now(tz=pytz.utc)
                    client_response_success = True
                    self.logger.info('successfully connected to url')
                    break
                except:
                    self.logger.info('failed to connect to url using selenium')
                    time.sleep(10)
                    continue

            if not client_response_success:
                raise Exception('unable to connect to url after retries')

        else:
            raise ValueError('unrecognised method {}'.format(method))

        return client_response, now

    def _get_source_html(self, client_response, method, close_client=True):
        self.logger.info('getting web page html (using method {})'
                         .format(method))
        if method == 'dynamic_client':
            source_html = client_response.mainFrame().toHtml()
            source_html = unicode(source_html)

        elif method == 'selenium':
            source_html = client_response.page_source
            if close_client:
                client_response.close()

        else:
            raise ValueError('unrecognised method {}'.format(method))

        return source_html

    def _get_comp_info_from_soup(self, soup):
        self.logger.info('getting competition info')

        comp_info = soup.find_all('div', id='breadcrumb-nav')
        assert (len(comp_info) == 1)
        comp_info = comp_info[0]

        comp_info_region = comp_info.find_all('span', class_='iconize')
        assert (len(comp_info_region) == 1)
        comp_info_region = comp_info_region[0].text.strip()

        comp_info = comp_info.find_all('a')
        assert (len(comp_info) == 1)
        comp_info = comp_info[0].text.strip()

        return {'region': comp_info_region, 'competition': comp_info}

    def _get_match_stats_from_soup(self, soup):
        self.logger.info('getting aggregated match stats')

        match_stats = soup.find_all('li', class_='match-centre-stat has-stats')

        match_stats_dict = {}  # initialise
        for match_stat in match_stats:
            data_for = match_stat.attrs['data-for']
            if data_for in match_stats_dict:
                continue

            match_stats_dict[data_for] = {}

            for ha in ['home', 'away']:
                match_stat_ha = match_stat.find_all(
                    'span', class_='match-centre-stat-value',
                    attrs={'data-field': ha})

                assert (len(match_stat_ha) == 1)
                match_stat_ha = match_stat_ha[0].text

                match_stats_dict[data_for][ha] = match_stat_ha
                # TODO: consider taking further details in within match centre stats

        return match_stats_dict

    # def _get_commentary_from_soup(self, soup):
    #     commentary_items = soup.find_all('li', class_='commentary-item')
    #
    #     commentary_list = []  # initialise
    #     for comm_item in commentary_items:
    #         attrs_ = comm_item.attrs
    #         attrs_['text'] = comm_item.text
    #         if 'class' in attrs_:
    #             attrs_.pop('class')
    #
    #         commentary_list += [attrs_]
    #     # TODO: try getting other panels of commentary items

    def _extract_var_from_scripts(self, var, scripts):
        self.logger.info('getting var {}'.format(var))

        var_ = '{} = '.format(var)

        data = [s for s in scripts if var_ in str(s)]
        assert (len(data) == 1)
        data = str(data[0])

        data = data.split('var ')
        data = [d for d in data if d.startswith(var_)]
        assert (len(data) == 1)
        data = data[0].replace('</script>', '')
        data = data.strip()

        if data.endswith(';'):
            data = data[:-1]  # remove ending semicolon

        data = data[len(var_):]  # remove starting var label
        data = json.loads(data)
        return data

    def _write_to_db(self, data_dict, fixture_id=None):
        self.logger.info('writing to data base')
        if self._db_type == 'mongo':
            self.db_connection.update(data_dict, data_dict, upsert=True)
        elif self._db_type == 'postgres':
            assert (fixture_id is not None)
            write_fixture_raw_data_to_db(
                db_cursor=self.db_connection.cursor(), fixture_id=fixture_id,
                data_dict=data_dict)
            try:
                self.db_connection.commit()
            except:
                self.db_connection.rollback()
        else:
            raise ValueError('unrecognised db_type {}'.format(self._db_type))


if __name__ == '__main__':
    # data = WhoScoredScraper.scrape_fixtures(621145, write_to_db=False)

    s = WhoScoredScraper(db_type='postgres')
    # fixture_ids = s.get_fixture_ids(comp='EngPr', season='2018/2019')
    # _ = WhoScoredScraper.scrape_fixtures(fixture_ids, write_to_db=True)
    _ = WhoScoredScraper.scrape_fixtures(1317601, write_to_db=False, db_type='postgres')

    for comp in ['EngPr', 'FraL1', 'GerBL1', 'ItaSA', 'SpaPr']:
        for season in ['2012/2013', '2013/2014', '2014/2015', '2015/2016',
                       '2016/2017', '2017/2018', '2018/2019']:
    # for comp in ['EngPr']:
    #     for season in ['2017/2018', '2018/2019']:
            fixture_ids = s.get_fixture_ids(comp=comp, season=season)
            for _ in range(1000):  # reties
                try:
                    _ = WhoScoredScraper.scrape_fixtures(fixture_ids,
                                                         write_to_db=True,
                                                         db_type='postgres')
                    break
                except:
                    time.sleep(20)
                    continue

            print '***** DONE SEASON *****'
        print '***** DONE COMP *****'

    # data = WhoScoredScraper.scrape_fixtures(1317601, write_to_db=True)
    # data = WhoScoredScraper.scrape_fixtures(1284899, write_to_db=True)

    # TODO: write a query to db to examine the number of fixtures scraped
    # TODO for each comp/season, and check it is as expected?
