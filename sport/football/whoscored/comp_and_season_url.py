#!/usr/bin/python

__author__ = 'psessford'


NO_REPORTS_MSG = ('no match reports found (last time check at least, '
                  'in early 2019)')


def get_comp_and_season_url(comp, season):
    """Get url for fixtures of a competition and season

    :param comp: (str) e.g. 'EngPr'
    :param season: (str) e.g. '2018/2019'

    :return url: (str)
    """
    if comp == 'ArgSL':
        region_id = 11
        tournament_id = 68

        if season == '2018/2019':
            season_id = 7460
            stage_id = None
        elif season == '2017/2018':
            season_id = 6995
            stage_id = None
        elif season == '2016/2017':
            season_id = 6484
            stage_id = None
        elif season == '2016':
            season_id = 6144
            stage_id = 13288
        elif season == '2015':
            raise ValueError(NO_REPORTS_MSG)
        else:
            raise ValueError('unsupported season {}'.format(season))

    elif comp == 'BraSA':
        region_id = 31
        tournament_id = 95
        stage_id = None

        if season == '2018':
            season_id = 7243
        elif season == '2017':
            season_id = 6700
        elif season == '2016':
            season_id = 6242
        elif season == '2015':
            season_id = 5713
        elif season == '2014':
            season_id = 4185
        elif season == '2013':
            season_id = 3753
        elif season == '2012':
            raise ValueError(NO_REPORTS_MSG)
        else:
            raise ValueError('unsupported season {}'.format(season))

    elif comp == 'ChiSL':
        region_id = 45
        tournament_id = 162
        stage_id = None

        if season == '2018':
            season_id = 7242
        elif season == '2017':
            season_id = 6685
        elif season == '2016':
            season_id = 6213
        elif season == '2015':
            raise ValueError(NO_REPORTS_MSG)
        else:
            raise ValueError('unsupported season {}'.format(season))

    elif comp == 'EngCh':
        region_id = 252
        tournament_id = 7

        if season == '2018/2019':
            season_id = 7379
            stage_id = None  # TODO: update after playoffs
        elif season == '2017/2018':
            season_id = 6848
            stage_id = 15177
        elif season == '2016/2017':
            season_id = 6365
            stage_id = 13832
        elif season == '2015/2016':
            season_id = 5827
            stage_id = 12497
        elif season == '2014/2015':
            season_id = 4312
            stage_id = 9156
        elif season == '2013/2014':
            season_id = 3859
            stage_id = 7800
        elif season == '2012/2013':
            raise ValueError(NO_REPORTS_MSG)
        else:
            raise ValueError('unsupported season {}'.format(season))

    elif comp == 'EngPr':
        region_id = 252
        tournament_id = 2
        stage_id = None

        if season == '2018/2019':
            season_id = 7361
        elif season == '2017/2018':
            season_id = 6829
        elif season == '2016/2017':
            season_id = 6335
        elif season == '2015/2016':
            season_id = 5826
        elif season == '2014/2015':
            season_id = 4311
        elif season == '2013/2014':
            season_id = 3853
        elif season == '2012/2013':
            season_id = 3389
        else:
            raise ValueError('unsupported season {}'.format(season))

    elif comp == 'FraL1':
        region_id = 74
        tournament_id = 22
        stage_id = None

        if season == '2018/2019':
            season_id = 7344
        elif season == '2017/2018':
            season_id = 6833
        elif season == '2016/2017':
            season_id = 6318
        elif season == '2015/2016':
            season_id = 5830
        elif season == '2014/2015':
            season_id = 4279
        elif season == '2013/2014':
            season_id = 3836
        elif season == '2012/2013':
            season_id = 3356
        else:
            raise ValueError('unsupported season {}'.format(season))

    elif comp == 'GerBL1':
        region_id = 81
        tournament_id = 3
        stage_id = None

        if season == '2018/2019':
            season_id = 7405
        elif season == '2017/2018':
            season_id = 6902
        elif season == '2016/2017':
            season_id = 6392
        elif season == '2015/2016':
            season_id = 5870
        elif season == '2014/2015':
            season_id = 4336
        elif season == '2013/2014':
            season_id = 3863
        elif season == '2012/2013':
            season_id = 3424
        else:
            raise ValueError('unsupported season {}'.format(season))

    elif comp == 'GerBL2':
        region_id = 81
        tournament_id = 6
        stage_id = None

        if season == '2018/2019':
            season_id = 7406
        elif season == '2017/2018':
            season_id = 6903
        elif season == '2016/2017':
            season_id = 6393
        elif season == '2015/2016':
            season_id = 5871
        elif season == '2014/2015':
            raise ValueError(NO_REPORTS_MSG)
        else:
            raise ValueError('unsupported season {}'.format(season))

    elif comp == 'ItaSA':
        region_id = 108
        tournament_id = 5
        stage_id = None

        if season == '2018/2019':
            season_id = 7468
        elif season == '2017/2018':
            season_id = 6974
        elif season == '2016/2017':
            season_id = 6461
        elif season == '2015/2016':
            season_id = 5970
        elif season == '2014/2015':
            season_id = 5441
        elif season == '2013/2014':
            season_id = 3978
        elif season == '2012/2013':
            season_id = 3512
        else:
            raise ValueError('unsupported season {}'.format(season))

    elif comp == 'NetED':
        region_id = 155
        tournament_id = 13

        if season == '2018/2019':
            season_id = 7354
            stage_id = None  # TODO: update after playoffs
        elif season == '2017/2018':
            season_id = 6826
            stage_id = 15148
        elif season == '2016/2017':
            season_id = 6331
            stage_id = 13792
        elif season == '2015/2016':
            season_id = 5810
            stage_id = 12467
        elif season == '2014/2015':
            season_id = 4289
            stage_id = 9121
        elif season == '2013/2014':
            season_id = 3851
            stage_id = 7790
        elif season == '2012/2013':
            raise ValueError(NO_REPORTS_MSG)
        else:
            raise ValueError('unsupported season {}'.format(season))

    elif comp == 'PorPL':
        region_id = 177
        tournament_id = 21
        stage_id = None

        if season == '2018/2019':
            season_id = 7429
        elif season == '2017/2018':
            season_id = 6933
        elif season == '2016/2017':
            season_id = 6438
        elif season == '2015/2016':
            raise ValueError(NO_REPORTS_MSG)
        else:
            raise ValueError('unsupported season {}'.format(season))

    elif comp == 'RusPL':
        region_id = 182
        tournament_id = 77
        stage_id = None

        if season == '2018/2019':
            season_id = 7389
        elif season == '2017/2018':
            season_id = 6819
        elif season == '2016/2017':
            season_id = 6357
        elif season == '2015/2016':
            season_id = 5859
        elif season == '2014/2015':
            season_id = 4303
        elif season == '2013/2014':
            season_id = 3861
        elif season == '2012/2013':
            raise ValueError(NO_REPORTS_MSG)
        else:
            raise ValueError('unsupported season {}'.format(season))

    elif comp == 'SpaPr':
        region_id = 206
        tournament_id = 4
        stage_id = None

        if season == '2018/2019':
            season_id = 7466
        elif season == '2017/2018':
            season_id = 6960
        elif season == '2016/2017':
            season_id = 6436
        elif season == '2015/2016':
            season_id = 5933
        elif season == '2014/2015':
            season_id = 5435
        elif season == '2013/2014':
            season_id = 3922
        elif season == '2012/2013':
            season_id = 3470
        else:
            raise ValueError('unsupported season {}'.format(season))

    elif comp == 'TurSL':
        region_id = 225
        tournament_id = 17
        stage_id = None

        if season == '2018/2019':
            season_id = 7437
        elif season == '2017/2018':
            season_id = 6936
        elif season == '2016/2017':
            season_id = 6447
        elif season == '2015/2016':
            season_id = 5915
        elif season == '2014/2015':
            season_id = 5411
        elif season == '2013/2014':
            raise ValueError(NO_REPORTS_MSG)
        else:
            raise ValueError('unsupported season {}'.format(season))

    elif comp == 'UsaMLS':
        region_id = 233
        tournament_id = 85

        if season == '2018':
            season_id = 7172
            stage_id = 15823
        elif season == '2017':
            season_id = 6620
            stage_id = 14550
        elif season == '2016':
            season_id = 6137
            stage_id = 13276
        elif season == '2015':
            season_id = 5607
            stage_id = 11890
        elif season == '2014':
            season_id = 4091
            stage_id = 8358
        elif season == '2013':
            season_id = 3672
            stage_id = 7250
        elif season == '2012':
            raise ValueError(NO_REPORTS_MSG)
        else:
            raise ValueError('unsupported season {}'.format(season))

    else:
        raise ValueError('unsupported comp {}'.format(comp))

    url = ('https://www.whoscored.com/Regions/{}/Tournaments/{}/Seasons/{}'
           .format(region_id, tournament_id, season_id))
    if stage_id is not None:
        url += ('/Stages/{}'.format(stage_id))

    return url
