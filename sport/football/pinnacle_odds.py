#!/usr/bin/env python

from datetime import datetime
import pytz
from sport.pinnacle_odds_base import PinnacleOddsBase


class FootballPinnacleOdds(PinnacleOddsBase):
    """Helper to scrape Pinnacle odds for football (soccer). Odds are for
    regular time.
    """
    def __init__(self, comp):
        super(FootballPinnacleOdds, self).__init__()

        if comp == 'EngPr':
            region = 'england'
            competition = 'england-premier-league'
        elif comp == 'FraL1':
            region = 'france'
            competition = 'ligue-1'
        elif comp == 'GerBL1':
            region = 'germany'
            competition = 'bundesliga'
        elif comp == 'ItaSA':
            region = 'italy'
            competition = 'italy-serie-a'
        elif comp == 'SpaPr':
            region = 'spain'
            competition = 'spain-la-liga'
        else:
            raise ValueError('unrecognised comp {}'.format(comp))

        self._comp = comp

        self._url = (
            'https://www.pinnacle.com/en/odds/match/soccer/{}/{}?sport=True'
        ).format(region, competition)

        self._scrape_tuples = [
            ('team_name', 'game-name name'),
            ('handicap', 'oddTip game-handicap'),
            ('totals', 'game-total oddTip ng-scope')
        ]
        # TODO: also take three-way odds

        self._req_fixture_info_len = [2, 3]

        self._supremacy_mkt_cols = [
            'handicap_spread', 'handicap_price'
        ]
        self._cols_to_numeric = [
            'team1_handicap_price', 'team2_handicap_price',
            'totals_over_price', 'totals_under_price',
        ]

    @property
    def sport(self):
        return 'football'


if __name__ == '__main__':
    comp = 'ItaSA'

    now = datetime.now(tz=pytz.utc)
    now = str(now).replace(' ', '_').replace('.', '_').replace(':', '-')

    file_path = ('/home/pat/02_web_scraping/scraped_data/sport/football/'
                 'pinnacle_odds/')
    file_path += '{}_{}_pn_odds.csv'.format(now, comp)

    odds = FootballPinnacleOdds(comp=comp)
    fixture_info_df = odds.scrape(write_to_db=False, csv_file=None)
    print fixture_info_df.head()
    print fixture_info_df.iloc[0]
