#!/usr/bin/env python

from datetime import datetime
import pytz
from sport.pinnacle_odds_base import PinnacleOddsBase


class IcehockeyPinnacleOdds(PinnacleOddsBase):
    """Helper to scrape Pinnacle odds for ice hockey. Odds include any
    over-time.
    """
    def __init__(self, comp):
        super(IcehockeyPinnacleOdds, self).__init__()

        if comp == 'UsaNHL':
            region = 'usa'
            competition = 'nhl'
        else:
            raise ValueError('unrecognised comp {}'.format(comp))

        self._comp = comp

        self._url = (
            'https://www.pinnacle.com/en/odds/match/hockey/{}/'
            '{}-ot-included?sport=True'
        ).format(region, competition)

        self._scrape_tuples = [
            ('team_name', 'game-name name'),
            ('moneyline', 'oddTip game-moneyline'),
            ('handicap', 'oddTip game-handicap'),
            ('totals', 'game-total oddTip ng-scope')
        ]

        self._req_fixture_info_len = [2]

        self._supremacy_mkt_cols = [
            'moneyline', 'handicap_spread', 'handicap_price'
        ]
        self._cols_to_numeric = [
            'team1_moneyline', 'team2_moneyline',
            'team1_handicap_price', 'team2_handicap_price',
            'totals_over_price', 'totals_under_price'
        ]

    @property
    def sport(self):
        return 'icehockey'


if __name__ == '__main__':
    # url = ('https://www.pinnacle.com/webapi/1.17/api/v1/GuestLines/'
    #        'Deadball/19/1460?callback=angular.callbacks._0')

    comp = 'UsaNHL'

    now = datetime.now(tz=pytz.utc)
    now = str(now).replace(' ', '_').replace('.', '_').replace(':', '-')

    file_path = ('/home/pat/02_web_scraping/scraped_data/sport/icehockey/'
                 'pinnacle_odds/')
    file_path += '{}_{}_pn_odds.csv'.format(now, comp)

    odds = IcehockeyPinnacleOdds(comp=comp)
    fixture_info_df = odds.scrape(write_to_db=False, csv_file=None)
    print fixture_info_df.head()
    print fixture_info_df.iloc[0]
