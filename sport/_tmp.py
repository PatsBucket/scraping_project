
def _write_to_db(rankings, search_term, search_engine, n_results, now,
                 wanted_sites_only=True):
    # db_client = MongoClient()

    import mysql.connector

    mydb = mysql.connector.connect(
        host="PatrickS.mysql.pythonanywhere-services.com",
        user="PatrickS",
        passwd="datadata",
        database="PatrickS$google_search_rankings"
    )

    mycursor = mydb.cursor()
    mycursor.execute("SHOW TABLES")
    mytables = [t[0] for t in mycursor.fetchall()]
    if 'rankings' not in mytables:
        print 'creating table'
        mycursor.execute('CREATE TABLE rankings ('
            'search_term VARCHAR(255), search_engine VARCHAR(255), '
            'datetime DATETIME, n_search_results INT, '
            'site_ranked VARCHAR(255), site_rank INT)')

    for k, v in rankings.iteritems():
        r = {
            'search_term': search_term,
            'search_engine': search_engine,
            'datetime': now,  # record datetime
            'n_search_results': n_results,
            'site_ranked': k,
            'site_rank': v,
        }
    #     db_client.google_search_rankings.rankings.update(r, r, upsert=True)

        try:
            sql = ('INSERT INTO rankings (search_term, search_engine, '
                   'datetime, n_search_results, site_ranked, site_rank) '
                   'VALUES (%s, %s, %s, %s, %s, %s)')
            val = (r['search_term'], r['search_engine'], r['datetime'],
                   r['n_search_results'], r['site_ranked'], r['site_rank'])
            mycursor.execute(sql, val)
            mydb.commit()
            print 'wrote {}'.format(r)
        except:
            mydb.rollback()

    mydb.close()
