#!/usr/bin/env python

import argparse
import time


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument(
        '-sport', required=True, help='e.g. football')

    arg_parser.add_argument(
        '-comp', nargs='+', required=True,
        help='e.g. EngPr if sport is football')
    # TODO: currently there is a "Segmentation fault (core dumped)" when
    # TODO looping through multiple comps, so this should be fixed

    arg_parser.add_argument('-write_to_db', default=False)

    arg_parser.add_argument(
        '-sleep_secs', required=False, default=120,
        help='seconds to wait between requests')
    args = arg_parser.parse_args()

    print ('requesting for sport {}, comp {} (with write_to_db {})'
           .format(args.sport, args.comp, args.write_to_db))

    if args.sport == 'football':
        from sport.football.pinnacle_odds import FootballPinnacleOdds

        for comp_ in args.comp:
            print 'running for comp {}'.format(comp_)
            odds = FootballPinnacleOdds(comp=comp_)
            _ = odds.scrape(write_to_db=args.write_to_db, csv_file=None)
            print 'finished {}'.format(comp_)
            time.sleep(args.sleep_secs)

    elif args.sport == 'icehockey':
        from sport.icehockey.pinnacle_odds import IcehockeyPinnacleOdds

        for comp_ in args.comp:
            print 'running for comp {}'.format(comp_)
            odds = IcehockeyPinnacleOdds(comp=comp_)
            _ = odds.scrape(write_to_db=args.write_to_db, csv_file=None)
            print 'finished {}'.format(comp_)
            time.sleep(args.sleep_secs)

    else:
        raise ValueError('unrecognised sport {}'.format(args.sport))
