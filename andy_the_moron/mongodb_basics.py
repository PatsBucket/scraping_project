
from pymongo import MongoClient


if __name__ == '__main__':
    db_client = MongoClient()
    print db_client

    my_db = db_client.my_db
    print my_db

    my_posts = my_db.posts
    print my_posts

    print my_posts.find_one()
    # my_posts.insert({'movie': 'Pulp Fiction'})

    print my_posts.find_one()

    print my_posts.find_one({'movie': 'Star Wars'})
    print my_posts.find_one({'movie': 'Pulp Fiction'})

    # my_posts.update({'movie': 'Moneyball'}, {'movie': {'title': 'Moneyball'}}, upsert=True)
