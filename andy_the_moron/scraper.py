
import requests
# Add import of BeautifulSoup
from bs4 import BeautifulSoup
import time

if __name__ == '__main__':
    response = requests.get("http://andythemoron.com")
    # Add use of BeautifulSoup to parse the response
    soup = BeautifulSoup(response.text, "lxml")
    # Let's print out the BeautifulSoup object for now instead
    print(soup)
